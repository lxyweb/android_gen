#!/usr/bin/env ruby
require 'thor'
require "json"
require "colored"

# http://whatisthor.com/
class MyCLI < Thor
  desc "hello NAME", "say hello to NAME"
  def hello(name)
    puts "Hello #{name}"
  end

  desc "g ActivityName", "Generate Activity"
  def gn(name)
  	puts "creating ..."
  end

  desc "g Model var_name1:var_type1 var_name2:var_type2", "Example app.rb g Person id:int  name:String"
  # 生成Model
  def g(name, *types)
    model_name = name.capitalize
  	puts "public class #{model_name} {"
    types.each do |type|
       field = type.split(':')
       puts "   public #{field[1]} #{field[0]};"
    end
    puts "}"
  end

  desc "p PermissionName", "p "
  # 搜索 Android权限
  def p(name)
    open("files/permission.json") do |f|
      permissions = JSON.parse(f.read)
      permissions.each do |permission|
        if permission[1].include?(name.upcase) or permission[0].include?(name.upcase) 
          puts permission[0].green
          puts permission[1].yellow
        end
      end
    end

  end


end

MyCLI.start(ARGV)